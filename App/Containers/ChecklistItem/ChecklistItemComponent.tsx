import React from 'react'
import styles from './AreaItemComponentStyle'
import { Text, TouchableOpacity } from 'react-native'
import { Icon, View } from 'native-base'

const ChecklistItemComponent = ({ 
    item,
}) => {

    const {
        name,
      } = item
  return (
    <TouchableOpacity 
        activeOpacity={0.8} 
        // onPress={onPressChecklist}
        style={{
            backgroundColor: '#DADADA',
            marginBottom:10,
        }}
    >
        <View>
            <Text>
                {name}
            </Text>
        </View>
    </TouchableOpacity>
  )
}

export default ChecklistItemComponent
