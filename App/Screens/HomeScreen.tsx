
import React, {Component} from 'react';
import {
    StatusBar, 
    View, 
    Text,
    FlatList,
    RefreshControl,
} from 'react-native';

import {
    Container,
    Content,
    Input,
    Button,
    Fab,
    Icon,
} from 'native-base'

import { Actions } from 'react-native-router-flux';
import ChecklistItemComponent from '../Containers/ChecklistItem/ChecklistItemComponent'
import { inject, observer } from 'mobx-react';

@inject('store')

@observer


export default class HomeScreen extends Component {

    constructor(props) {
		super(props);

		this.state = {
            checklist_data:[]
            
        }

    }

    fetchChecklist = () => {

        const { store } = this.props

        store.checklistStore.getChecklist()
        .then(() => {

            this.setState({
                checklist_data: store.checklistStore.checklist_data
            })
        })
    }
    componentDidMount(){
        
        this.fetchChecklist()
    }

    render() {

        return (
            <View
                style={{
                    padding: 20,
                    flex:1,
                }}
            >
                <FlatList
                    data={this.state.checklist_data}
                    extraData={this.state}
                    
                    // refreshControl={
                    //     <RefreshControl
                    //         style={{backgroundColor: 'transparent'}}
                    //         refreshing={this.state.refreshing}
                    //         onRefresh={this._onRefresh}
                        
                    //     />
                        
                    // }
                    renderItem = {({item, index}) => 
                        
                        
                        <ChecklistItemComponent 
                            item = {item}
                            
                        />
                        

                    }
                />
                <Fab
                    // active={true}
                    direction="up"
                    containerStyle={{ }}
                    // style={{ backgroundColor:  }}
                    position="bottomRight"
                    // onPress={() => this.addChecklist()}
                >
                    <Icon
                        type={'MaterialIcons'} 
                        name="playlist-add-check" 
                    />
                    
                </Fab>
                
            </View>
        );
    }
  }
